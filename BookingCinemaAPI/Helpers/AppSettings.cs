﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Helpers
{
    public class AppSettings
    {
        public string SecretKey { get; set; }
        public string ConnectionString { get; set; }
        public string JsonRoute { get; set; }
        public string Descripton { get; set; }
        public string UIEndpoint { get; set; }
    }
    public struct Type
    {
        public const string ERROR = "Error";
        public const string SUCCESS = "Success";

    }
}
