﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class CumRapVM
    {
        public string maCumRap { get; set; }
        public string tenCumRap { get; set; }
        public string ThongTin { get; set; }

        public string maHeThongRap { get; set; }

        public List<RapVM> danhSachRap { get; set; }

        public List<LichChieuVM> lichChieuPhim { get; set; }
    }
}
