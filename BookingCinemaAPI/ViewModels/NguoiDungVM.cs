﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class NguoiDungVM
    {
        public string taiKhoan { get; set; }
        public string matKhau { get; set; }
        public string email { get; set; }
        public string soDt { get; set; }
        public string hoTen { get; set; }
        public string accessToken { get; set; }
        public string maLoaiNguoiDung { get; set; }
    }
}
