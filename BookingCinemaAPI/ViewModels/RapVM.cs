﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class RapVM
    {
        public int maRap { get; set; }
        public string tenRap { get; set; }
        public string maTrangThaiRap { get; set; }
        public string maCumRap { get; set; }
    }
}
