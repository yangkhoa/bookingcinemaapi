﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class PhimVM
    {
        public int maPhim { get; set; }
        public string tenPhim { get; set; }
        public string maLoaiPhim { get; set; }
        public string biDanh { get; set; }
        public string trailer { get; set; }
        public string hinhAnh { get; set; }
        public string moTa { get; set; }
        public string ngayKhoiChieu { get; set; }
        public string danhGia { get; set; }
    }
}
