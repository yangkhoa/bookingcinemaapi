﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class DanhSachVeDatVM
    {
        public int maLichChieu { get; set; }
        public List<VeVM> danhSachVe { get; set; }
        public string taiKhoanNguoiDung { get; set; }

        public string email { get; set; }
        public string soDT { get; set; }

    }
}
