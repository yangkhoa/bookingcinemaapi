﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class ThongTinDangNhapVM
    {
        public string taiKhoan { get; set; }
        public string matKhau { get; set; }
    }
}
