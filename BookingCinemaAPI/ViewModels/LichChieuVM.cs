﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class LichChieuVM
    {
        public int maLichChieu { get; set; }
        public DateTime ngayChieuGioChieu { get; set; }
        public double giaVe { get; set; }
        public int thoiLuong { get; set; }
        public int maRap { get; set; }
        public string tenRap { get; set; }
    }
}






