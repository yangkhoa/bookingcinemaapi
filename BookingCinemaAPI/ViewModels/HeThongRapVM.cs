﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.ViewModels
{
    public class HeThongRapVM
    {
        public string maHeThongRap { get; set; }
        public string tenHeThongRap { get; set; }
        public string tenNgan { get; set; }
        public string hinhLogo { get; set; }

        public List<CumRapVM> cumRapChieu { get; set; }
    }
}
