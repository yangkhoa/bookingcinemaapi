﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class DatVe
    {
        public DatVe()
        {
            ChiTietDatVe = new HashSet<ChiTietDatVe>();
        }

        public int MaDatVe { get; set; }
        public DateTime? NgayDat { get; set; }
        public string TaiKhoan { get; set; }
        public double? GiamGia { get; set; }
        public string Email { get; set; }
        public string SoDienThoai { get; set; }
        public int? TrangThai { get; set; }
        public int? DaXoa { get; set; }
        public double? TongTien { get; set; }

        public virtual NguoiDung TaiKhoanNavigation { get; set; }
        public virtual ICollection<ChiTietDatVe> ChiTietDatVe { get; set; }
    }
}
