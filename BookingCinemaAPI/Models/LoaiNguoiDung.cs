﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class LoaiNguoiDung
    {
        public LoaiNguoiDung()
        {
            NguoiDung = new HashSet<NguoiDung>();
        }

        public string MaLoaiNguoiDung { get; set; }
        public string TenLoai { get; set; }
        public int? DaXoa { get; set; }

        public virtual ICollection<NguoiDung> NguoiDung { get; set; }
    }
}
