﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class ChiTietDatVe
    {
        public int Id { get; set; }
        public int MaDatVe { get; set; }
        public int MaLichChieu { get; set; }
        public string MaLoaiGhe { get; set; }
        public int? DaXoa { get; set; }

        public virtual DatVe MaDatVeNavigation { get; set; }
        public virtual LichChieu MaLichChieuNavigation { get; set; }
        public virtual LoaiGhe MaLoaiGheNavigation { get; set; }
    }
}
