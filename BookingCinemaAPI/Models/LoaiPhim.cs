﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class LoaiPhim
    {
        public LoaiPhim()
        {
            Phim = new HashSet<Phim>();
        }

        public string MaLoaiPhim { get; set; }
        public string TenLoaiPhim { get; set; }
        public int? DaXoa { get; set; }

        public virtual ICollection<Phim> Phim { get; set; }
    }
}
