﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class NguoiDung
    {
        public NguoiDung()
        {
            DatVe = new HashSet<DatVe>();
            Ghe = new HashSet<Ghe>();
        }

        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string Email { get; set; }
        public string SoDienThoai { get; set; }
        public string HoTen { get; set; }
        public string TokenKey { get; set; }
        public string MaLoaiNguoiDung { get; set; }
        public int? DaXoa { get; set; }

        public virtual LoaiNguoiDung MaLoaiNguoiDungNavigation { get; set; }
        public virtual ICollection<DatVe> DatVe { get; set; }
        public virtual ICollection<Ghe> Ghe { get; set; }
    }
}
