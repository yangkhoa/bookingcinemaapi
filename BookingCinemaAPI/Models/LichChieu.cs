﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class LichChieu
    {
        public LichChieu()
        {
            ChiTietDatVe = new HashSet<ChiTietDatVe>();
        }

        public int MaLichChieu { get; set; }
        public DateTime? NgayChieuGioChieu { get; set; }
        public double? GiaVe { get; set; }
        public int? ThoiLuong { get; set; }
        public int MaRap { get; set; }
        public int MaPhim { get; set; }
        public int? DaXoa { get; set; }

        public virtual Phim MaPhimNavigation { get; set; }
        public virtual Rap MaRapNavigation { get; set; }
        public virtual ICollection<ChiTietDatVe> ChiTietDatVe { get; set; }
    }
}
