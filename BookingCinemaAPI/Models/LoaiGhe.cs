﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class LoaiGhe
    {
        public LoaiGhe()
        {
            ChiTietDatVe = new HashSet<ChiTietDatVe>();
            Ghe = new HashSet<Ghe>();
        }

        public string MaLoaiGhe { get; set; }
        public string TenLoaiGhe { get; set; }
        public string MoTa { get; set; }
        public int? DaXoa { get; set; }
        public double? GiaVe { get; set; }

        public virtual ICollection<ChiTietDatVe> ChiTietDatVe { get; set; }
        public virtual ICollection<Ghe> Ghe { get; set; }
    }
}
