﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class HeThongRap
    {
        public HeThongRap()
        {
            CumRap = new HashSet<CumRap>();
        }

        public string MaHeThongRap { get; set; }
        public string TenHeThongRap { get; set; }
        public string TenNgan { get; set; }
        public string HinhLogo { get; set; }
        public int? DaXoa { get; set; }

        public virtual ICollection<CumRap> CumRap { get; set; }
    }
}
