﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BookingCinemaAPI.Models
{
    public partial class ANGULARCINEMAContext : DbContext
    {
        public ANGULARCINEMAContext()
        {
        }

        public ANGULARCINEMAContext(DbContextOptions<ANGULARCINEMAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ChiTietDatVe> ChiTietDatVe { get; set; }
        public virtual DbSet<CumRap> CumRap { get; set; }
        public virtual DbSet<DatVe> DatVe { get; set; }
        public virtual DbSet<Ghe> Ghe { get; set; }
        public virtual DbSet<HeThongRap> HeThongRap { get; set; }
        public virtual DbSet<LichChieu> LichChieu { get; set; }
        public virtual DbSet<LoaiGhe> LoaiGhe { get; set; }
        public virtual DbSet<LoaiNguoiDung> LoaiNguoiDung { get; set; }
        public virtual DbSet<LoaiPhim> LoaiPhim { get; set; }
        public virtual DbSet<NguoiDung> NguoiDung { get; set; }
        public virtual DbSet<Phim> Phim { get; set; }
        public virtual DbSet<Rap> Rap { get; set; }
        public virtual DbSet<TrangThaiGhe> TrangThaiGhe { get; set; }
        public virtual DbSet<TrangThaiRap> TrangThaiRap { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=45.76.157.27;Initial Catalog=ANGULARCINEMA;Persist Security Info=True;User ID=khoa;Password=dev123!#");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChiTietDatVe>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MaDatVe).HasColumnName("maDatVe");

                entity.Property(e => e.MaLichChieu).HasColumnName("maLichCHieu");

                entity.Property(e => e.MaLoaiGhe)
                    .IsRequired()
                    .HasColumnName("maLoaiGhe")
                    .HasMaxLength(50);

                entity.HasOne(d => d.MaDatVeNavigation)
                    .WithMany(p => p.ChiTietDatVe)
                    .HasForeignKey(d => d.MaDatVe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ChiTietDa__daXoa__59063A47");

                entity.HasOne(d => d.MaLichChieuNavigation)
                    .WithMany(p => p.ChiTietDatVe)
                    .HasForeignKey(d => d.MaLichChieu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ChiTietDa__maLic__59FA5E80");

                entity.HasOne(d => d.MaLoaiGheNavigation)
                    .WithMany(p => p.ChiTietDatVe)
                    .HasForeignKey(d => d.MaLoaiGhe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ChiTietDa__maLoa__5AEE82B9");
            });

            modelBuilder.Entity<CumRap>(entity =>
            {
                entity.HasKey(e => e.MaCumRap)
                    .HasName("PK__CumRap__988740F151C4E9C2");

                entity.Property(e => e.MaCumRap)
                    .HasColumnName("maCumRap")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MaHeThongRap)
                    .IsRequired()
                    .HasColumnName("maHeThongRap")
                    .HasMaxLength(50);

                entity.Property(e => e.TenCumRap)
                    .HasColumnName("tenCumRap")
                    .HasMaxLength(200);

                entity.Property(e => e.ThongTin).HasColumnName("thongTin");

                entity.HasOne(d => d.MaHeThongRapNavigation)
                    .WithMany(p => p.CumRap)
                    .HasForeignKey(d => d.MaHeThongRap)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CumRap__maHeThon__2E1BDC42");
            });

            modelBuilder.Entity<DatVe>(entity =>
            {
                entity.HasKey(e => e.MaDatVe)
                    .HasName("PK__DatVe__299568C1505472AB");

                entity.Property(e => e.MaDatVe).HasColumnName("maDatVe");

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(200);

                entity.Property(e => e.GiamGia)
                    .HasColumnName("giamGia")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NgayDat)
                    .HasColumnName("ngayDat")
                    .HasColumnType("datetime");

                entity.Property(e => e.SoDienThoai)
                    .HasColumnName("soDienThoai")
                    .HasMaxLength(10);

                entity.Property(e => e.TaiKhoan)
                    .IsRequired()
                    .HasColumnName("taiKhoan")
                    .HasMaxLength(100);

                entity.Property(e => e.TongTien).HasColumnName("tongTien");

                entity.Property(e => e.TrangThai).HasColumnName("trangThai");

                entity.HasOne(d => d.TaiKhoanNavigation)
                    .WithMany(p => p.DatVe)
                    .HasForeignKey(d => d.TaiKhoan)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DatVe__daXoa__5535A963");
            });

            modelBuilder.Entity<Ghe>(entity =>
            {
                entity.HasKey(e => e.MaGhe)
                    .HasName("PK__Ghe__2D87404CBA591F5B");

                entity.Property(e => e.MaGhe)
                    .HasColumnName("maGhe")
                    .ValueGeneratedNever();

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MaLoaiGhe)
                    .IsRequired()
                    .HasColumnName("maLoaiGhe")
                    .HasMaxLength(50);

                entity.Property(e => e.MaRap).HasColumnName("maRap");

                entity.Property(e => e.MaTrangThaiGhe)
                    .HasColumnName("maTrangThaiGhe")
                    .HasMaxLength(50);

                entity.Property(e => e.Stt)
                    .HasColumnName("STT")
                    .HasMaxLength(50);

                entity.Property(e => e.TaiKhoan)
                    .HasColumnName("taiKhoan")
                    .HasMaxLength(100);

                entity.Property(e => e.TenGhe)
                    .HasColumnName("tenGhe")
                    .HasMaxLength(200);

                entity.HasOne(d => d.MaLoaiGheNavigation)
                    .WithMany(p => p.Ghe)
                    .HasForeignKey(d => d.MaLoaiGhe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Ghe__maLoaiGhe__4222D4EF");

                entity.HasOne(d => d.MaRapNavigation)
                    .WithMany(p => p.Ghe)
                    .HasForeignKey(d => d.MaRap)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Ghe__daXoa__403A8C7D");

                entity.HasOne(d => d.MaTrangThaiGheNavigation)
                    .WithMany(p => p.Ghe)
                    .HasForeignKey(d => d.MaTrangThaiGhe)
                    .HasConstraintName("FK__Ghe__maTrangThai__412EB0B6");

                entity.HasOne(d => d.TaiKhoanNavigation)
                    .WithMany(p => p.Ghe)
                    .HasForeignKey(d => d.TaiKhoan)
                    .HasConstraintName("FK__Ghe__taiKhoan__6E01572D");
            });

            modelBuilder.Entity<HeThongRap>(entity =>
            {
                entity.HasKey(e => e.MaHeThongRap)
                    .HasName("PK__HeThongR__EBD5D63653C450FD");

                entity.Property(e => e.MaHeThongRap)
                    .HasColumnName("maHeThongRap")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.HinhLogo).HasColumnName("hinhLogo");

                entity.Property(e => e.TenHeThongRap)
                    .HasColumnName("tenHeThongRap")
                    .HasMaxLength(200);

                entity.Property(e => e.TenNgan)
                    .HasColumnName("tenNgan")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LichChieu>(entity =>
            {
                entity.HasKey(e => e.MaLichChieu)
                    .HasName("PK__LichChie__6ADC2032ADB9DB25");

                entity.Property(e => e.MaLichChieu).HasColumnName("maLichChieu");

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.GiaVe)
                    .HasColumnName("giaVe")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MaPhim).HasColumnName("maPhim");

                entity.Property(e => e.MaRap).HasColumnName("maRap");

                entity.Property(e => e.NgayChieuGioChieu)
                    .HasColumnName("ngayChieuGioChieu")
                    .HasColumnType("datetime");

                entity.Property(e => e.ThoiLuong)
                    .HasColumnName("thoiLuong")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.MaPhimNavigation)
                    .WithMany(p => p.LichChieu)
                    .HasForeignKey(d => d.MaPhim)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LichChieu__maPhi__5070F446");

                entity.HasOne(d => d.MaRapNavigation)
                    .WithMany(p => p.LichChieu)
                    .HasForeignKey(d => d.MaRap)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LichChieu__daXoa__4E88ABD4");
            });

            modelBuilder.Entity<LoaiGhe>(entity =>
            {
                entity.HasKey(e => e.MaLoaiGhe)
                    .HasName("PK__LoaiGhe__441CD3014F95F427");

                entity.Property(e => e.MaLoaiGhe)
                    .HasColumnName("maLoaiGhe")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.GiaVe).HasColumnName("giaVe");

                entity.Property(e => e.MoTa).HasColumnName("moTa");

                entity.Property(e => e.TenLoaiGhe)
                    .HasColumnName("tenLoaiGhe")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<LoaiNguoiDung>(entity =>
            {
                entity.HasKey(e => e.MaLoaiNguoiDung)
                    .HasName("PK__LoaiNguo__E7EBD5D51710A769");

                entity.Property(e => e.MaLoaiNguoiDung)
                    .HasColumnName("maLoaiNguoiDung")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TenLoai)
                    .HasColumnName("tenLoai")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<LoaiPhim>(entity =>
            {
                entity.HasKey(e => e.MaLoaiPhim)
                    .HasName("PK__LoaiPhim__A45F2B256E1B256D");

                entity.Property(e => e.MaLoaiPhim)
                    .HasColumnName("maLoaiPhim")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TenLoaiPhim)
                    .HasColumnName("tenLoaiPhim")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<NguoiDung>(entity =>
            {
                entity.HasKey(e => e.TaiKhoan)
                    .HasName("PK__NguoiDun__B4C45319D88A32AB");

                entity.Property(e => e.TaiKhoan)
                    .HasColumnName("taiKhoan")
                    .HasMaxLength(100);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(200);

                entity.Property(e => e.HoTen)
                    .HasColumnName("hoTen")
                    .HasMaxLength(200);

                entity.Property(e => e.MaLoaiNguoiDung)
                    .IsRequired()
                    .HasColumnName("maLoaiNguoiDung")
                    .HasMaxLength(50);

                entity.Property(e => e.MatKhau)
                    .HasColumnName("matKhau")
                    .HasMaxLength(100);

                entity.Property(e => e.SoDienThoai)
                    .HasColumnName("soDienThoai")
                    .HasMaxLength(20);

                entity.Property(e => e.TokenKey)
                    .HasColumnName("tokenKey")
                    .IsUnicode(false);

                entity.HasOne(d => d.MaLoaiNguoiDungNavigation)
                    .WithMany(p => p.NguoiDung)
                    .HasForeignKey(d => d.MaLoaiNguoiDung)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NguoiDung__maLoa__276EDEB3");
            });

            modelBuilder.Entity<Phim>(entity =>
            {
                entity.HasKey(e => e.MaPhim)
                    .HasName("PK__Phim__9F38F6301ABA56B9");

                entity.Property(e => e.MaPhim).HasColumnName("maPhim");

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DanhGia).HasColumnName("danhGia");

                entity.Property(e => e.HinhLogo).HasColumnName("hinhLogo");

                entity.Property(e => e.LinkTrailer).HasColumnName("linkTrailer");

                entity.Property(e => e.MaLoaiPhim)
                    .IsRequired()
                    .HasColumnName("maLoaiPhim")
                    .HasMaxLength(50);

                entity.Property(e => e.MoTa).HasColumnName("moTa");

                entity.Property(e => e.NgayKhoiChieu)
                    .HasColumnName("ngayKhoiChieu")
                    .HasColumnType("datetime");

                entity.Property(e => e.TenNgan)
                    .HasColumnName("tenNgan")
                    .HasMaxLength(255);

                entity.Property(e => e.TenPhim)
                    .HasColumnName("tenPhim")
                    .HasMaxLength(200);

                entity.HasOne(d => d.MaLoaiPhimNavigation)
                    .WithMany(p => p.Phim)
                    .HasForeignKey(d => d.MaLoaiPhim)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Phim__maLoaiPhim__48CFD27E");
            });

            modelBuilder.Entity<Rap>(entity =>
            {
                entity.HasKey(e => e.MaRap)
                    .HasName("PK__Rap__27B172A180E14B4F");

                entity.Property(e => e.MaRap).HasColumnName("maRap");

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MaCumRap)
                    .HasColumnName("maCumRap")
                    .HasMaxLength(50);

                entity.Property(e => e.MaTrangThaiRap)
                    .IsRequired()
                    .HasColumnName("maTrangThaiRap")
                    .HasMaxLength(50);

                entity.Property(e => e.SoGhe)
                    .HasColumnName("soGhe")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TenRap)
                    .HasColumnName("tenRap")
                    .HasMaxLength(200);

                entity.HasOne(d => d.MaCumRapNavigation)
                    .WithMany(p => p.Rap)
                    .HasForeignKey(d => d.MaCumRap)
                    .HasConstraintName("FK__Rap__daXoa__35BCFE0A");

                entity.HasOne(d => d.MaTrangThaiRapNavigation)
                    .WithMany(p => p.Rap)
                    .HasForeignKey(d => d.MaTrangThaiRap)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rap__maTrangThai__36B12243");
            });

            modelBuilder.Entity<TrangThaiGhe>(entity =>
            {
                entity.HasKey(e => e.MaTrangThaiGhe)
                    .HasName("PK__TrangTha__BB44F35E5AC76FE5");

                entity.Property(e => e.MaTrangThaiGhe)
                    .HasColumnName("maTrangThaiGhe")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MoTa).HasColumnName("moTa");

                entity.Property(e => e.TenTrangThaiGhe)
                    .HasColumnName("tenTrangThaiGhe")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TrangThaiRap>(entity =>
            {
                entity.HasKey(e => e.MaTrangThaiRap)
                    .HasName("PK__TrangTha__B6C921DC1C8D7C1A");

                entity.Property(e => e.MaTrangThaiRap)
                    .HasColumnName("maTrangThaiRap")
                    .HasMaxLength(50);

                entity.Property(e => e.DaXoa)
                    .HasColumnName("daXoa")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MoTa).HasColumnName("moTa");

                entity.Property(e => e.TenTrangThaiRap)
                    .HasColumnName("tenTrangThaiRap")
                    .HasMaxLength(200);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
