﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class Phim
    {
        public Phim()
        {
            LichChieu = new HashSet<LichChieu>();
        }

        public int MaPhim { get; set; }
        public string TenPhim { get; set; }
        public string LinkTrailer { get; set; }
        public string HinhLogo { get; set; }
        public string MoTa { get; set; }
        public DateTime? NgayKhoiChieu { get; set; }
        public string DanhGia { get; set; }
        public string MaLoaiPhim { get; set; }
        public int? DaXoa { get; set; }
        public string TenNgan { get; set; }

        public virtual LoaiPhim MaLoaiPhimNavigation { get; set; }
        public virtual ICollection<LichChieu> LichChieu { get; set; }
    }
}
