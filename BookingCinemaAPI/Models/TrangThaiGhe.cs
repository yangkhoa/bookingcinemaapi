﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class TrangThaiGhe
    {
        public TrangThaiGhe()
        {
            Ghe = new HashSet<Ghe>();
        }

        public string MaTrangThaiGhe { get; set; }
        public string TenTrangThaiGhe { get; set; }
        public string MoTa { get; set; }
        public int? DaXoa { get; set; }

        public virtual ICollection<Ghe> Ghe { get; set; }
    }
}
