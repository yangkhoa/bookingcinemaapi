﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class Ghe
    {
        public string Stt { get; set; }
        public int MaGhe { get; set; }
        public string TenGhe { get; set; }
        public string MaTrangThaiGhe { get; set; }
        public int MaRap { get; set; }
        public string MaLoaiGhe { get; set; }
        public int? DaXoa { get; set; }
        public string TaiKhoan { get; set; }
        public bool daDat => (TaiKhoan != null ? true : false);
        public virtual LoaiGhe MaLoaiGheNavigation { get; set; }
        public virtual Rap MaRapNavigation { get; set; }
        public virtual TrangThaiGhe MaTrangThaiGheNavigation { get; set; }
        public virtual NguoiDung TaiKhoanNavigation { get; set; }
    }
}
