﻿using System;
using System.Collections.Generic;

namespace BookingCinemaAPI.Models
{
    public partial class TrangThaiRap
    {
        public TrangThaiRap()
        {
            Rap = new HashSet<Rap>();
        }

        public string MaTrangThaiRap { get; set; }
        public string TenTrangThaiRap { get; set; }
        public string MoTa { get; set; }
        public int? DaXoa { get; set; }

        public virtual ICollection<Rap> Rap { get; set; }
    }
}
