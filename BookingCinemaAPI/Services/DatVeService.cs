﻿using BookingCinemaAPI.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.ViewModels;

namespace BookingCinemaAPI.Services
{
    public class DatVeService: IDatVeService
    {
        public dynamic GetDanhSachPhongVe(int maLichChieu)
        {
            dynamic objReturn = null;
            var db = new ANGULARCINEMAContext();
            objReturn = new
            {
                thongTinPhim = (from lc in db.LichChieu
                                join phim in db.Phim on lc.MaPhim equals phim.MaPhim
                                join rap in db.Rap on lc.MaRap equals rap.MaRap
                                join cumrap in db.CumRap on rap.MaCumRap equals cumrap.MaCumRap
                                where lc.MaPhim == phim.MaPhim 
                                && lc.MaRap == rap.MaRap
                                && rap.MaCumRap == cumrap.MaCumRap 
                                && lc.MaLichChieu == maLichChieu
                                && lc.DaXoa ==0
                                select new
                                {
                                    maLichChieu = lc.MaLichChieu,
                                    tenCumRap = cumrap.TenCumRap,
                                    tenRap = rap.TenRap,
                                    diaChi = cumrap.ThongTin,
                                    tenPhim = phim.TenPhim,
                                    hinhAnh = phim.HinhLogo,
                                    ngayChieu = Convert.ToDateTime(lc.NgayChieuGioChieu).Date.ToString("dd/MM/yyyy"),
                                    gioChieu = Convert.ToDateTime(lc.NgayChieuGioChieu).ToString("hh:mm")

                                }).FirstOrDefault(),
                danhSachGhe = (from lc in db.LichChieu
                               join phim in db.Phim on lc.MaPhim equals phim.MaPhim
                               join rap in db.Rap on lc.MaRap equals rap.MaRap
                               join ghe in db.Ghe on rap.MaRap equals ghe.MaRap
                               join loaighe in db.LoaiGhe on ghe.MaLoaiGhe equals loaighe.MaLoaiGhe
                               where lc.MaPhim == phim.MaPhim && 
                               lc.MaRap == rap.MaRap &&
                               rap.MaRap == ghe.MaRap &&
                               ghe.MaLoaiGhe == loaighe.MaLoaiGhe && 
                               lc.MaLichChieu == maLichChieu && 
                               lc.DaXoa == 0
                               select new
                               {
                                   maGhe = ghe.MaGhe,
                                   tenGhe = ghe.TenGhe,
                                   maRap = ghe.MaRap,
                                   loaiGhe = loaighe.MaLoaiGhe,
                                   stt = ghe.Stt,
                                   giaVe = loaighe.GiaVe,
                                   daDat = ghe.daDat,
                                   taiKhoanNguoiDat = ghe.TaiKhoan,
                               })
            };
            return objReturn;
        }

        public DatVe DatVe(DanhSachVeDatVM thongTinDatVe)
        {
            using(var db = new ANGULARCINEMAContext())
            {
                double tongTien = 0;
                DatVe model = new DatVe
                {
                    NgayDat = DateTime.Now,
                    TaiKhoan = thongTinDatVe.taiKhoanNguoiDung,
                    Email = thongTinDatVe.email,
                    SoDienThoai = thongTinDatVe.soDT,
                    TrangThai = 0,
                };

                db.DatVe.Add(model);

                db.SaveChanges();

                int idDatVe = model.MaDatVe;

                if (idDatVe > 0)
                {
                    foreach(VeVM ve in thongTinDatVe.danhSachVe)
                    {
                        ChiTietDatVe ctModel = new ChiTietDatVe()
                        {
                            MaDatVe = idDatVe,
                            MaLichChieu = thongTinDatVe.maLichChieu,
                            MaLoaiGhe = ve.maLoaiGhe,
                        };

                        db.ChiTietDatVe.Add(ctModel);

                        var ghe = db.Ghe.Find(ve.maGhe);
                        ghe.TaiKhoan = thongTinDatVe.taiKhoanNguoiDung;

                        var loaiGhe = db.LoaiGhe.Find(ve.maLoaiGhe);

                        tongTien += (double)loaiGhe.GiaVe;
                    }
                    model.TongTien = tongTien;
                    db.Update(model);
                }

                var success = db.SaveChanges();
                if (success > 0) return model;

                return null;
            }
        }

        public dynamic GetFullListTypeSeat()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                dynamic objReturn = (from lp in db.LoaiGhe
                                     where lp.DaXoa == 0
                                     select new
                                     {
                                         maLoaiGhe = lp.MaLoaiGhe,
                                         tenLoaiGhe = lp.TenLoaiGhe,
                                         giaVe = lp.GiaVe
                                     }).ToList();

                return objReturn;
            }
        }
    }
}
