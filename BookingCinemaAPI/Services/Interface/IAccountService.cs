﻿using BookingCinemaAPI.Models;
using BookingCinemaAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Services.Interface
{
    public interface IAccountService
    {
        NguoiDungVM Authenticate(string username, string password);
        List<NguoiDungVM> GetListUser();
        NguoiDungVM CheckUser(string taiKhoan, string soDienThoai, string Email);
        string UpdateNguoiDung(NguoiDungVM nguoiDung, int type);
        dynamic GetFullTypeUser();
        NguoiDungVM FindUser(string taiKhoan);
        dynamic GetDataDashboard();
    }
}
