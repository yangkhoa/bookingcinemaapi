﻿using BookingCinemaAPI.Models;
using BookingCinemaAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Services.Interface
{
    public interface IDatVeService
    {
        dynamic GetDanhSachPhongVe(int maLichChieu);
        DatVe DatVe(DanhSachVeDatVM thongTinDatVe);

        dynamic GetFullListTypeSeat();
    }
}
