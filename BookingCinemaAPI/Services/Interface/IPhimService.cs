﻿using BookingCinemaAPI.Models;
using BookingCinemaAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Services.Interface
{
    public interface IPhimService
    {
        List<PhimVM> GetListPhim();

        PhimVM GetPhimByCode(int maPhim);

        List<PhimVM> GetListPhimComming();

        List<PhimVM> GetFullListPhim();
        
        string UpdatePhim(PhimVM phim, int type);

        Phim CheckPhim(int maPhim);

        dynamic GetFullTypePhim();
    }
}
