﻿using BookingCinemaAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Services.Interface
{
    public interface IRapService
    {
        HeThongRapVM GetHeThongRapByCode(string codeHTR);
        List<HeThongRapVM> GetListHeThongRap();
        List<CumRapVM> GetCumRapByCode(string codeHTR);
        dynamic GetLichChieuByMovie(int maPhim);
        dynamic GetLichChieuByHeThongRap(string maHTR = null);
        string UpdateRap(RapVM rap, int type);
        List<RapVM> GetListRap();
        RapVM GetRapById(int maRap);
        dynamic GetFullTypeRap();
        List<CumRapVM> GetListCumRap();
        string UpdateCumRap(CumRapVM cumRap, int type);
        CumRapVM GetCumRapByMCR(string maCumRap);
    }
}
