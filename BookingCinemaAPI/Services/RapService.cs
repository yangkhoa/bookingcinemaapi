﻿using BookingCinemaAPI.Helpers;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Services
{
    public class RapService : IRapService
    {
        private readonly AppSettings _appSettings;

        public RapService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public HeThongRapVM GetHeThongRapByCode(string codeHTR)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                HeThongRap htr = db.HeThongRap.FirstOrDefault(x => x.MaHeThongRap == codeHTR && x.DaXoa ==0);
                if (htr != null)
                    return new HeThongRapVM
                    {
                        maHeThongRap = htr.MaHeThongRap,
                        tenHeThongRap = htr.TenHeThongRap,
                        tenNgan = htr.TenNgan,
                        hinhLogo = htr.HinhLogo
                    };
                return null;
            }
        }

        public List<HeThongRapVM> GetListHeThongRap()
        {
            List<HeThongRapVM> listHTR = null;
            using (var db = new ANGULARCINEMAContext())
            {
                listHTR = (from htr in db.HeThongRap
                           where htr.DaXoa ==0
                           select new HeThongRapVM
                           {
                               maHeThongRap = htr.MaHeThongRap,
                               tenHeThongRap = htr.TenHeThongRap,
                               tenNgan = htr.TenNgan,
                               hinhLogo = htr.HinhLogo

                           }).ToList();
                return listHTR;
            }
        }

        public List<CumRapVM> GetCumRapByCode(string codeHTR)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                HeThongRap htr = db.HeThongRap.FirstOrDefault(x => x.MaHeThongRap == codeHTR);

                if (htr != null)
                {
                    List<CumRapVM> listCumRap = (from s in db.CumRap
                                           where s.MaHeThongRap == htr.MaHeThongRap && s.DaXoa ==0

                                           select new CumRapVM
                                           {
                                               maCumRap = s.MaCumRap,
                                               tenCumRap = s.TenCumRap,
                                               ThongTin = s.ThongTin
                                           }).ToList();
                    if (listCumRap != null)
                    {
                        foreach(var cr in listCumRap)
                        {
                            List<RapVM> listRap = (from r in db.Rap
                                             where r.MaCumRap == cr.maCumRap && r.DaXoa ==0
                                             select new RapVM
                                             {
                                                 maRap = r.MaRap,
                                                 tenRap = r.TenRap
                                             }).ToList();
                            if (listRap != null)
                            {
                                cr.danhSachRap = listRap;
                            }

                        }
                        return listCumRap;
                    }                 
                }
                return null;
            }
        }

        public dynamic GetLichChieuByMovie(int maPhim)
        {
            dynamic objReturn = null;
            using (var db = new ANGULARCINEMAContext())
            {
                //Phim phim = db.Phim.Find(maPhim);

                Phim phim = db.Phim.FirstOrDefault(x => x.MaPhim == maPhim && x.DaXoa == 0);

                objReturn = new
                {
                    heThongRapChieu = (from htr in db.HeThongRap
                                       where htr.DaXoa == 0
                                       select new
                                       {
                                           cumRapChieu = (from cr in db.CumRap
                                                          where cr.MaHeThongRap == htr.MaHeThongRap && cr.DaXoa == 0
                                                          select new
                                                          {
                                                              lichChieuPhim = (from lc in db.LichChieu
                                                                               join m in db.Rap on lc.MaRap equals m.MaRap
                                                                               where lc.MaRap == m.MaRap && lc.DaXoa == 0
                                                                               && lc.MaPhim == maPhim
                                                                               && cr.MaCumRap == m.MaCumRap

                                                                               select new
                                                                               {
                                                                                   maLichChieu = lc.MaLichChieu,
                                                                                   maRap = lc.MaRap,
                                                                                   tenRap = m.TenRap,
                                                                                   ngayChieuGioChieu = lc.NgayChieuGioChieu,
                                                                                   giaVe = lc.GiaVe,
                                                                                   thoiLuong = lc.ThoiLuong
                                                                               }).ToList(),
                                                              maCumRap = cr.MaCumRap,
                                                              tenCumRap = cr.TenCumRap
                                                          }).ToList(),
                                           maHeThongRap = htr.MaHeThongRap,
                                           tenHeThongRap = htr.TenHeThongRap
                                       }).ToList().Where(x=>x.cumRapChieu.Where(y=>y.lichChieuPhim.Count>0).ToList().Any()).ToList(),
                    maPhim = phim.MaPhim,
                    tenPhim = phim.TenPhim,
                    biDanh = phim.TenNgan,
                    trailer = phim.LinkTrailer,
                    hinhAnh = phim.HinhLogo,
                    moTa = phim.MoTa,
                    ngayKhoiChieu = phim.NgayKhoiChieu,
                    danhGia = phim.DanhGia,
                };
            }

            return objReturn;
        }

        public dynamic GetLichChieuByHeThongRap(string maHTR=null)
        {
            dynamic objReturn = null;
            ANGULARCINEMAContext db = new ANGULARCINEMAContext();

            if (maHTR != null)
            {
                objReturn = (from htr in db.HeThongRap
                             where htr.MaHeThongRap == maHTR && htr.DaXoa ==0
                             select new
                             {
                                 maHeThongRap = htr.MaHeThongRap,
                                 tenHeThongRap = htr.TenHeThongRap,
                                 lstCumRap = (from cr in db.CumRap
                                              where cr.MaHeThongRap == htr.MaHeThongRap && cr.DaXoa == 0
                                              select new
                                              {
                                                  maCumRap = cr.MaCumRap,
                                                  tenCumRap = cr.TenCumRap,
                                                  diaChi = cr.ThongTin,
                                                  danhSachPhim = (from p in db.Phim
                                                                  select new
                                                                  {
                                                                      maPhim = p.MaPhim,
                                                                      tenPhim = p.TenPhim,
                                                                      hinhAnh = p.HinhLogo,
                                                                      loaiPhim = p.MaLoaiPhim,
                                                                      lichChieuPhim = (from lc in db.LichChieu
                                                                                       join m in db.Rap on lc.MaRap equals m.MaRap
                                                                                       where lc.MaRap == m.MaRap
                                                                                       && lc.MaPhim == p.MaPhim
                                                                                       && cr.MaCumRap == m.MaCumRap
                                                                                       && lc.DaXoa == 0
                                                                                       select new
                                                                                       {
                                                                                           maLichChieu = lc.MaLichChieu,
                                                                                           maRap = lc.MaRap,
                                                                                           tenRap = m.TenRap,
                                                                                           ngayChieuGioChieu = lc.NgayChieuGioChieu,
                                                                                           giaVe = lc.GiaVe,
                                                                                           thoiLuong = lc.ThoiLuong
                                                                                       }).ToList(),
                                                                  }).ToList()
                                              }).ToList()
                             }).ToList().Where(x => x.lstCumRap.Where(y => y.danhSachPhim.Where(z => z.lichChieuPhim.Count > 0).Any()).Any()).ToList();

                return objReturn;
            }

            objReturn = (from htr in db.HeThongRap
                         where htr.DaXoa ==0
                         select new
                         {
                             maHeThongRap = htr.MaHeThongRap,
                             tenHeThongRap = htr.TenHeThongRap,
                             lstCumRap = (from cr in db.CumRap
                                          where cr.MaHeThongRap == htr.MaHeThongRap && cr.DaXoa ==0
                                          select new
                                          {
                                              maCumRap = cr.MaCumRap,
                                              tenCumRap = cr.TenCumRap,
                                              diaChi = cr.ThongTin,
                                              danhSachPhim = (from p in db.Phim
                                                              where p.DaXoa ==0
                                                              select new
                                                              {
                                                                  maPhim = p.MaPhim,
                                                                  tenPhim = p.TenPhim,
                                                                  hinhAnh = p.HinhLogo,
                                                                  loaiPhim = p.MaLoaiPhim,
                                                                  lichChieuPhim = (from lc in db.LichChieu
                                                                                   join m in db.Rap on lc.MaRap equals m.MaRap
                                                                                   where lc.MaRap == m.MaRap
                                                                                   && lc.MaPhim == p.MaPhim
                                                                                   && cr.MaCumRap == m.MaCumRap
                                                                                   && lc.DaXoa == 0
                                                                                   select new
                                                                                   {
                                                                                       maLichChieu = lc.MaLichChieu,
                                                                                       maRap = lc.MaRap,
                                                                                       tenRap = m.TenRap,
                                                                                       ngayChieuGioChieu = lc.NgayChieuGioChieu,
                                                                                       giaVe = lc.GiaVe,
                                                                                       thoiLuong = lc.ThoiLuong
                                                                                   }).ToList(),
                                                              }).ToList()
                                          }).ToList()
                         }).ToList().Where(x => x.lstCumRap.Where(y => y.danhSachPhim.Where(z => z.lichChieuPhim.Count > 0).Any()).Any()).ToList();
            db.Dispose();
            return objReturn;
        }

        /// <summary>
        /// 0 --> Tạo mới
        /// 1 --> Cập nhật
        /// 3 --> Xóa
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string UpdateRap(RapVM rap, int type)
        {
            string msg = string.Empty;
            try
            {
                using (var db = new ANGULARCINEMAContext())
                {
                    if (type == 0)
                    {
                        Rap model = new Rap
                        {
                            MaCumRap =  rap.maCumRap,
                            MaTrangThaiRap  = rap.maTrangThaiRap,
                            TenRap = rap.tenRap
                        };

                        db.Rap.Add(model);

                    }
                    if (type == 1)
                    {
                        Rap rapModel = db.Rap.FirstOrDefault(x => x.MaRap == rap.maRap && x.DaXoa == 0);
                        if (rapModel != null)
                        {
                            rapModel.MaCumRap = rap.maCumRap;
                            rapModel.MaTrangThaiRap = rap.maTrangThaiRap;
                            rapModel.TenRap = rap.tenRap;
                        }
                        db.Rap.Update(rapModel);
                    }

                    if (type == 2)
                    {
                        Rap rapModel = db.Rap.FirstOrDefault(x => x.MaRap == rap.maRap && x.DaXoa == 0);
                        if (rapModel != null)
                        {
                            rapModel.DaXoa = 1;
                        }
                        db.Rap.Update(rapModel);
                    }

                    int rs = db.SaveChanges();
                    if (rs > 0)
                        msg = Helpers.Type.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
            }
            return msg;
        }

        public RapVM GetRapById(int maRap)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var rap = (from s in db.Rap
                            where s.MaRap == maRap
                            && s.DaXoa == 0
                            select new RapVM
                            {
                                maRap = s.MaRap,
                                tenRap = s.TenRap,
                                maTrangThaiRap = s.MaTrangThaiRap,
                                maCumRap = s.MaCumRap
                            }).FirstOrDefault();

                return rap;
            }
        }

        public List<RapVM> GetListRap()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var rap = (from s in db.Rap
                           where  s.DaXoa == 0
                           select new RapVM
                           {
                               maRap = s.MaRap,
                               tenRap = s.TenRap,
                               maTrangThaiRap = s.MaTrangThaiRap,
                               maCumRap = s.MaCumRap
                           });

                return rap.ToList();
            }
        }

        public dynamic GetFullTypeRap()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                dynamic objReturn = (from lp in db.TrangThaiRap
                                     where lp.DaXoa == 0
                                     select new
                                     {
                                         maTrangThaiRap = lp.MaTrangThaiRap,
                                         tenTrangThaiRap = lp.TenTrangThaiRap
                                     }).ToList();

                return objReturn;
            }
        }

        public List<CumRapVM> GetListCumRap()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var cumRap = (from s in db.CumRap
                           where s.DaXoa == 0
                           select new CumRapVM
                           {
                               tenCumRap = s.TenCumRap,
                               maCumRap = s.MaCumRap,
                               ThongTin = s.ThongTin,
                               maHeThongRap = s.MaHeThongRap
                           });

                return cumRap.ToList();
            }
        }

        public CumRapVM GetCumRapByMCR(string maCumRap)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var cumRap = (from s in db.CumRap
                              where s.MaCumRap.Trim().ToUpper() == maCumRap.Trim().ToUpper()
                              && s.DaXoa == 0
                              select new CumRapVM
                              {
                                  tenCumRap = s.TenCumRap,
                                  maCumRap = s.MaCumRap,
                                  ThongTin = s.ThongTin,
                                  maHeThongRap = s.MaHeThongRap
                              }).FirstOrDefault();

                return cumRap;
            }
        }

        /// <summary>
        /// 0 --> Tạo mới
        /// 1 --> Cập nhật
        /// 3 --> Xóa
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string UpdateCumRap(CumRapVM cumRap, int type)
        {
            string msg = string.Empty;
            try
            {
                using (var db = new ANGULARCINEMAContext())
                {
                    if (type == 0)
                    {
                        CumRap model = new CumRap
                        {
                            MaCumRap = cumRap.maCumRap,
                            TenCumRap = cumRap.tenCumRap,
                            ThongTin = cumRap.ThongTin,
                            MaHeThongRap = cumRap.maHeThongRap
                        };

                        db.CumRap.Add(model);

                    }
                    if (type == 1)
                    {
                        CumRap cumRapModel = db.CumRap.FirstOrDefault(x => string.Equals(x.MaCumRap,cumRap.maCumRap)==true && x.DaXoa == 0);
                        if (cumRapModel != null)
                        {
                            cumRapModel.MaCumRap = cumRap.maCumRap;
                            cumRapModel.TenCumRap = cumRap.tenCumRap;
                            cumRapModel.ThongTin = cumRap.ThongTin;
                            cumRapModel.MaHeThongRap = cumRap.maHeThongRap;
                        }
                        db.CumRap.Update(cumRapModel);
                    }

                    if (type == 2)
                    {
                        CumRap cumRapModel = db.CumRap.FirstOrDefault(x => string.Equals(x.MaCumRap, cumRap.maCumRap) == true && x.DaXoa == 0);
                        if (cumRapModel != null)
                        {
                            cumRapModel.DaXoa = 1;
                        }
                        db.CumRap.Update(cumRapModel);
                    }

                    int rs = db.SaveChanges();
                    if (rs > 0)
                        msg = Helpers.Type.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
            }
            return msg;
        }
    }
}
