﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BookingCinemaAPI.Helpers;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BookingCinemaAPI.Services
{
    public class AccountService: IAccountService
    {
        private readonly AppSettings _appSettings;

        public AccountService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public List<NguoiDungVM> GetListUser()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var listUser = from s in db.NguoiDung
                               join m in db.LoaiNguoiDung on s.MaLoaiNguoiDung equals m.MaLoaiNguoiDung
                               where s.MaLoaiNguoiDung == m.MaLoaiNguoiDung
                               && s.DaXoa == 0
                               select new NguoiDungVM
                               {
                                   taiKhoan = s.TaiKhoan,
                                   email = s.Email,
                                   maLoaiNguoiDung = s.MaLoaiNguoiDung,
                                   soDt = s.SoDienThoai,
                                   hoTen = s.HoTen

                               };

                return listUser.ToList();
            }
        }

        public NguoiDungVM FindUser(string taiKhoan)
        {
            using(var db = new ANGULARCINEMAContext())
            {
                
                return (from s in db.NguoiDung
                               join m in db.LoaiNguoiDung on s.MaLoaiNguoiDung equals m.MaLoaiNguoiDung
                               where s.MaLoaiNguoiDung == m.MaLoaiNguoiDung 
                               && s.TaiKhoan == taiKhoan
                               && s.DaXoa == 0
                               select new NguoiDungVM
                               {
                                   taiKhoan = s.TaiKhoan,
                                   email = s.Email,
                                   maLoaiNguoiDung = s.MaLoaiNguoiDung,
                                   soDt = s.SoDienThoai,
                                   hoTen = s.HoTen

                               }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 0 --> Tạo mới
        /// 1 --> Cập nhật
        /// 3 --> Xóa
        /// </summary>
        /// <param name="nguoiDung"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string UpdateNguoiDung(NguoiDungVM nguoiDung,int type)
        {
            string msg = string.Empty;
            try
            {
                using (var db = new ANGULARCINEMAContext())
                {
                    if (type == 0)
                    {
                        NguoiDung model = new NguoiDung
                        {
                            TaiKhoan = nguoiDung.taiKhoan,
                            MatKhau = nguoiDung.matKhau,
                            Email = nguoiDung.email,
                            SoDienThoai = nguoiDung.soDt,
                            HoTen = nguoiDung.hoTen,
                            MaLoaiNguoiDung = nguoiDung.maLoaiNguoiDung
                        };

                        db.NguoiDung.Add(model);
                       
                    }
                    if (type == 1)
                    {
                        NguoiDung user = db.NguoiDung.FirstOrDefault(x => x.TaiKhoan == nguoiDung.taiKhoan && x.DaXoa ==0);
                        if (user != null)
                        {
                            user.HoTen = nguoiDung.hoTen;
                            user.SoDienThoai = nguoiDung.soDt;
                            user.Email = nguoiDung.email;
                        }
                        db.NguoiDung.Update(user);
                    }

                    if (type == 2)
                    {
                        NguoiDung user = db.NguoiDung.FirstOrDefault(x => x.TaiKhoan == nguoiDung.taiKhoan && x.DaXoa==0);
                        if (user != null)
                        {
                            user.DaXoa = 1;
                        }
                        db.NguoiDung.Update(user);
                    }

                    int rs = db.SaveChanges();
                    if (rs > 0)
                        msg = Helpers.Type.SUCCESS;
                }
            }
            catch(Exception ex) 
            {
                msg = ex.Message.ToString();
            }
            return msg;
        }

        public NguoiDungVM CheckUser(string taiKhoan,string soDienThoai,string Email)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                NguoiDung user = db.NguoiDung.FirstOrDefault(x => x.TaiKhoan == taiKhoan && x.DaXoa ==0);

                if (user != null)
                    return new NguoiDungVM
                    {
                        taiKhoan = user.TaiKhoan,
                        soDt = user.SoDienThoai,
                        email = user.Email,
                        accessToken = user.TokenKey
                    };
                return null;
            }
        }

        public NguoiDungVM Authenticate(string username, string password)
        {
            NguoiDungVM user = null;
            using (var db = new ANGULARCINEMAContext())
            {
                user = (from s in db.NguoiDung
                        join m in db.LoaiNguoiDung on s.MaLoaiNguoiDung equals m.MaLoaiNguoiDung
                        where s.MaLoaiNguoiDung == m.MaLoaiNguoiDung && s.TaiKhoan == username && s.MatKhau == password
                        select new NguoiDungVM
                        {
                            taiKhoan = s.TaiKhoan,
                            email = s.Email,
                            maLoaiNguoiDung = s.MaLoaiNguoiDung,
                            soDt = s.SoDienThoai,
                            accessToken = s.TokenKey,
                            hoTen = s.HoTen

                        }).ToList().FirstOrDefault();
            }

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.taiKhoan.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.accessToken = tokenHandler.WriteToken(token);

            return user;
        }

        public dynamic GetFullTypeUser()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                dynamic objReturn = (from lp in db.LoaiNguoiDung
                                     where lp.DaXoa == 0
                                     select new
                                     {
                                         maLoaiNguoiDung = lp.MaLoaiNguoiDung,
                                         tenLoaiNguoiDung = lp.TenLoai
                                     }).ToList();

                return objReturn;
            }
        }
        
        public dynamic GetDataDashboard()
        {
            dynamic obj;
            double maxValue = 0;
            List<double> sum = new List<double>();
            using (var db = new ANGULARCINEMAContext())
            {
                int soLuongNguoiDung = db.NguoiDung.Count(x=>x.DaXoa ==0);
                int soLuongRap = db.Rap.Count(x => x.DaXoa == 0);
                int soLuongPhim = db.Phim.Count(x => x.DaXoa == 0);
                int soLuongCumRap = db.CumRap.Count(x => x.DaXoa == 0);
                for (int i = 1; i <= 12; i++)
                {
                    double item = (double)(from s in db.DatVe
                                        where s.NgayDat.Value.Month == i && s.DaXoa == 0
                                        select s.TongTien).Sum();
                    if (maxValue < item)
                    {
                        maxValue = item;
                    }
                    sum.Add(item);

                }
                obj = new
                {
                    soLuongNguoiDung,
                    soLuongRap,
                    soLuongPhim,
                    soLuongCumRap,
                    maxValue,
                    sum
                };
                
        
            }
            return obj;
        }
    }
}
