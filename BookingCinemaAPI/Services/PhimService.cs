﻿using BookingCinemaAPI.Helpers;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace BookingCinemaAPI.Services
{
    public class PhimService: IPhimService
    {
        private readonly AppSettings _appSettings;

        public PhimService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public dynamic GetFullTypePhim()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                dynamic objReturn = (from lp in db.LoaiPhim
                                     where lp.DaXoa == 0
                                     select new
                                     {
                                         maLoaiPhim = lp.MaLoaiPhim,
                                         tenLoaiPhim = lp.TenLoaiPhim
                                     }).ToList();

                return objReturn;
            }
        }

        public List<PhimVM> GetFullListPhim()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var listPhim = from s in db.Phim
                               join m in db.LoaiPhim on s.MaLoaiPhim equals m.MaLoaiPhim
                               where s.MaLoaiPhim == m.MaLoaiPhim &&
                               s.DaXoa == 0
                               
                               select new PhimVM
                               {
                                   maPhim = s.MaPhim,
                                   tenPhim = s.TenPhim,
                                   biDanh = s.TenNgan,
                                   hinhAnh = s.HinhLogo,
                                   trailer = s.LinkTrailer,
                                   danhGia = s.DanhGia,
                                   moTa = s.MoTa,
                                   ngayKhoiChieu = Convert.ToString(s.NgayKhoiChieu),
                                   maLoaiPhim = s.MaLoaiPhim

                               };

                return listPhim.ToList();
            }
        }

        public List<PhimVM> GetListPhim()
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var listPhim = from s in db.Phim
                               join m in db.LoaiPhim on s.MaLoaiPhim equals m.MaLoaiPhim
                               where s.MaLoaiPhim == m.MaLoaiPhim 
                               && s.NgayKhoiChieu <= DateTime.Now
                               && s.DaXoa == 0
                               select new PhimVM
                               {
                                   maPhim = s.MaPhim,
                                   tenPhim = s.TenPhim,
                                   biDanh = s.TenNgan,
                                   hinhAnh = s.HinhLogo,
                                   trailer = s.LinkTrailer,
                                   danhGia =s.DanhGia,
                                   moTa = s.MoTa,
                                   ngayKhoiChieu = Convert.ToString(s.NgayKhoiChieu),
                                   maLoaiPhim = s.MaLoaiPhim

                               };

                return listPhim.ToList();
            }
        }

        public PhimVM GetPhimByCode(int maPhim)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                var phim = (from s in db.Phim
                               join m in db.LoaiPhim on s.MaLoaiPhim equals m.MaLoaiPhim
                               where s.MaLoaiPhim == m.MaLoaiPhim 
                               && s.MaPhim == maPhim
                               && s.DaXoa == 0
                            

                            select new PhimVM
                               {
                                   maPhim = s.MaPhim,
                                   tenPhim = s.TenPhim,
                                   biDanh = s.TenNgan,
                                   hinhAnh = s.HinhLogo,
                                   trailer = s.LinkTrailer,
                                   danhGia = s.DanhGia,
                                   moTa = s.MoTa,
                                   ngayKhoiChieu = Convert.ToString(s.NgayKhoiChieu),
                                   maLoaiPhim = s.MaLoaiPhim

                               }).FirstOrDefault();

                return phim;
            }
        }

        public List<PhimVM> GetListPhimComming()
        {
            using (var db = new ANGULARCINEMAContext())
            {

                var listPhimSapChieu = from s in db.Phim
                               join m in db.LoaiPhim on s.MaLoaiPhim equals m.MaLoaiPhim
                               where s.MaLoaiPhim == m.MaLoaiPhim 
                               && s.NgayKhoiChieu > DateTime.Now
                               && s.DaXoa == 0
                               select new PhimVM
                               {
                                   maPhim = s.MaPhim,
                                   tenPhim = s.TenPhim,
                                   biDanh = s.TenNgan,
                                   hinhAnh = s.HinhLogo,
                                   trailer = s.LinkTrailer,
                                   danhGia = s.DanhGia,
                                   moTa = s.MoTa,
                                   ngayKhoiChieu = Convert.ToString(s.NgayKhoiChieu),
                                   maLoaiPhim = s.MaLoaiPhim

                               };

                return listPhimSapChieu.ToList();
            }
        }

        /// <summary>
        /// 0 --> Tạo mới
        /// 1 --> Cập nhật
        /// 3 --> Xóa
        /// </summary>
        /// <param name="phim"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string UpdatePhim(PhimVM phim, int type)
        {
            string msg = string.Empty;
            try
            {
                using (var db = new ANGULARCINEMAContext())
                {
                    if (type == 0)
                    {
                        Phim model = new Phim
                        {
                            TenNgan = phim.biDanh,
                            TenPhim = phim.tenPhim,
                            DanhGia = phim.danhGia.ToString(),
                            MaLoaiPhim = phim.maLoaiPhim,
                            HinhLogo = phim.hinhAnh,
                            LinkTrailer = phim.trailer,
                            MoTa = phim.moTa,
                            NgayKhoiChieu = DateTime.ParseExact(phim.ngayKhoiChieu, "dd/MM/yyyy", null),
                            DaXoa = 0
                        };

                        db.Phim.Add(model);
                    }
                    if (type == 1)
                    {
                        Phim phimModel = db.Phim.FirstOrDefault(x => x.MaPhim == phim.maPhim && x.DaXoa == 0);

                        if (phimModel != null)
                        {
                            phimModel.TenPhim = phim.tenPhim;
                            phimModel.DanhGia = phim.danhGia.ToString();
                            phimModel.HinhLogo = phim.hinhAnh;
                            phimModel.MaLoaiPhim = phim.maLoaiPhim;
                            phimModel.TenNgan = phim.biDanh;
                            phimModel.LinkTrailer = phim.trailer;
                            phimModel.MoTa = phim.moTa;
                            phimModel.NgayKhoiChieu = DateTime.ParseExact(phim.ngayKhoiChieu, "dd/MM/yyyy", null);
                        }
                        db.Phim.Update(phimModel);
                    }

                    if (type == 2)
                    {
                        Phim phimModel = db.Phim.FirstOrDefault(x => x.MaPhim == phim.maPhim && x.DaXoa == 0);
                        if (phimModel != null)
                        {
                            phimModel.DaXoa = 1;
                        }
                        db.Phim.Update(phimModel);
                    }

                    int rs = db.SaveChanges();
                    if (rs > 0) msg = Helpers.Type.SUCCESS;
                }
            }
            catch(Exception ex)
            {
                msg = ex.Message.ToString();
            }

            return msg;
        }

        public Phim CheckPhim(int maPhim)
        {
            using (var db = new ANGULARCINEMAContext())
            {
                return db.Phim.FirstOrDefault(x => x.MaPhim == maPhim && x.DaXoa == 0);
            }
        }
    }   
}
