﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingCinemaAPI.Models;
using Microsoft.Extensions.Logging;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;
using Microsoft.AspNetCore.Cors;

namespace BookingCinemaAPI.Controllers
{
    [Route("api/QuanLyNguoiDung")]
    [ApiController]
    public class NguoiDungController : ControllerBase
    {
        private readonly IAccountService Repository;

        public NguoiDungController(IAccountService repository)
        {
            this.Repository = repository;
        }

        [HttpGet]
        public IActionResult Test()
        {
            return BadRequest("Tài khoản hoặc mật khẩu không được để trống!!!");
        }    

        // GET: api/QuanLyNguoiDung
        [HttpGet("LayDanhSachNguoiDung")]
        public IActionResult GetNguoiDung(string taiKhoan)
        {
            List<NguoiDungVM> listNguoiDung = null;
            if (taiKhoan != null)
            {
                var user = Repository.FindUser(taiKhoan);
                return Ok(user);
            }
            listNguoiDung = Repository.GetListUser();


            if (listNguoiDung == null)
            {
                return BadRequest("Đã có lỗi xảy ra xin vui lòng thử lại sau!!!");
            }

            return Ok(listNguoiDung);
        }

        [HttpPost("DangNhap")]
        public IActionResult Login(ThongTinDangNhapVM nguoiDung)
        {
            try
            {
                if (nguoiDung.matKhau == string.Empty || nguoiDung.taiKhoan == string.Empty)
                {
                    return BadRequest("Tài khoản hoặc mật khẩu không được để trống!!!");
                }

                NguoiDungVM userReturn = Repository.Authenticate(nguoiDung.taiKhoan, nguoiDung.matKhau);

                if (userReturn == null)
                {
                    return BadRequest("Tài khoản hoặc mật khẩu chưa chính xác");
                }

                return Ok(userReturn);
            }
            catch
            {
                return BadRequest("Đã có lỗi xảy ra xin vui lòng thử lại sau!!!");
            }
        }

        [HttpPost("DangKy")]
        public IActionResult Register(NguoiDungVM nguoiDung)
        {
            string msg = string.Empty;
            try
            {
                var currentUser = Repository.CheckUser(nguoiDung.taiKhoan, nguoiDung.soDt, nguoiDung.email);

                if (currentUser != null)
                {
                    return BadRequest("Tài khoản đã được đăng ký");
                }

                msg = Repository.UpdateNguoiDung(nguoiDung, 0);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentUser);

                return BadRequest(msg);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }

        [HttpPut("CapNhatThongTinNguoiDung")]
        public IActionResult Update(NguoiDungVM nguoiDung)
        {
            string msg = string.Empty;
            try
            {
                var currentUser = Repository.CheckUser(nguoiDung.taiKhoan, nguoiDung.soDt, nguoiDung.email);

                if (currentUser == null)
                {
                    return BadRequest("Người dùng này chưa được tạo trong hệ thống");
                }

                msg = Repository.UpdateNguoiDung(nguoiDung, 1);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentUser);

                return BadRequest(msg);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }

        [HttpDelete("XoaNguoiDung")]
        public IActionResult Delete(string TaiKhoan)
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(TaiKhoan))
            {
                return BadRequest("Tài khoản không được bỏ trống");
            }

            NguoiDungVM nguoiDung = new NguoiDungVM
            {
                taiKhoan = TaiKhoan
            };

            msg = Repository.UpdateNguoiDung(nguoiDung, 2);

            if (msg == Helpers.Type.SUCCESS) return Ok(nguoiDung);

            return BadRequest(msg);
        }

        [HttpGet("LayDanhSachLoaiNguoiDung")]
        public IActionResult GetListTypePhim()
        {
            var listTypeUser = Repository.GetFullTypeUser();

            if (listTypeUser == null) return BadRequest("Lấy danh sách loại người dùng thất bại! Xin vui lòng thử lại");

            return Ok(listTypeUser);
        }

        [HttpGet("LayThongTinDashboard")]
        public IActionResult GetDataDashboad()
        {
            return Ok(Repository.GetDataDashboard());
        }
    }
}
