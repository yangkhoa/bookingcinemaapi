﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;

namespace BookingCinemaAPI.Controllers
{
    [Route("api/QuanLyDatVe")]
    [ApiController]
    public class DatVeController : ControllerBase
    {
        private readonly IDatVeService Repository;

        public DatVeController(IDatVeService repository)
        {
            this.Repository = repository;
        }

        [HttpGet("LayDanhSachPhongVe")]
        public IActionResult GetDanhSachPhongVe(int MaLichChieu)
        {
            if (MaLichChieu > 0)
            {
                var data = Repository.GetDanhSachPhongVe(MaLichChieu);
                return Ok(data);
            }
            return BadRequest("Mã lịch chiếu không hợp lệ!");

        }

        [HttpGet("LayDanhSachLoaiGhe")]
        public IActionResult GetListTypeSeat()
        {
            var listTypeSeat = Repository.GetFullListTypeSeat();

            if (listTypeSeat == null) return BadRequest("Lấy danh sách loại ghế thất bại! Xin vui lòng thử lại");

            return Ok(listTypeSeat);
        }

        [HttpPost("DatVe")]
        public IActionResult DatVe(DanhSachVeDatVM thongTin)
        {
            bool isSuccess = false;

            if (thongTin.maLichChieu < 0)
            {
                return BadRequest("Mã lịch chiếu không hợp lệ!");
            }

            var model = Repository.DatVe(thongTin);
            if (model!=null)
            {
                isSuccess = true;
            }
            if (isSuccess) return Ok(thongTin);

            return BadRequest("Đã có lỗi xảy ra xin vui lòng thử lại sau!!!");

        }

    }
}
