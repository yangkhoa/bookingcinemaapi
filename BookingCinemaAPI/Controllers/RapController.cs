﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;

namespace BookingCinemaAPI.Controllers
{
    [Route("api/QuanLyRap")]
    [ApiController]
    public class RapController : ControllerBase
    {
        private readonly IRapService Repository;

        public RapController(IRapService repository)
        {
            this.Repository = repository;
        }

        // GET: api/Rap
        [HttpGet("LayThongTinHeThongRap")]
        public IActionResult GetRap(string maHeThongRap)
        {
            if (string.IsNullOrEmpty(maHeThongRap))
            {
                return Ok(Repository.GetListHeThongRap());
            }
            return Ok(Repository.GetHeThongRapByCode(maHeThongRap));
        }

        [HttpGet("LayThongTinCumRapTheoHeThong")]
        public IActionResult GetListCumRap(string maHeThongRap)
        {
            if (string.IsNullOrEmpty(maHeThongRap)) return BadRequest("Mã hệ thống rạp không được để trống");

            List<CumRapVM> listCumRap = Repository.GetCumRapByCode(maHeThongRap);

            if (listCumRap == null) return NotFound("Đã có xảy ra xin vui lòng thử lại sau!!!");

            return Ok(listCumRap);
        }

        [HttpGet("LayThongTinLichChieuHeThongRap")]
        public IActionResult GetLichChieu(string maHeThongRap)
        {
            dynamic listHTR = null;
            if (!string.IsNullOrEmpty(maHeThongRap))
            {
                listHTR = Repository.GetLichChieuByHeThongRap(maHeThongRap);
            }
            else
            {
                listHTR = Repository.GetLichChieuByHeThongRap();
            }

            if (listHTR == null) return BadRequest("Đã có xảy ra xin vui lòng thử lại sau!!!");

            return Ok(listHTR);
        }

        [HttpGet("LayThongTinLichChieuPhim")]
        public IActionResult GetLichChieuByMovie(int maPhim)
        {
            if (maPhim==0) return BadRequest("Mã phim không tồn tại!");

            dynamic listCumRap = Repository.GetLichChieuByMovie(maPhim);

            if (listCumRap == null) return BadRequest("Đã có xảy ra xin vui lòng thử lại sau!!!");

            return Ok(listCumRap);
        }

        [HttpGet("LayThongTinRapChieu")]
        public IActionResult GetListRap(int maRap)
        {
            if (maRap!=0)
            {
                return Ok(Repository.GetRapById(maRap));
            }

            return Ok(Repository.GetListRap());
        }

        [HttpPost("ThemRapChieu")]
        public IActionResult ThemRapChieu(RapVM rap)
        {
            try
            {
                var currentRap = Repository.GetRapById(rap.maRap);

                if (currentRap != null)
                {
                    return BadRequest("Thông tin rạp này đã tồn tại trong hệ thống! Xin hãy kiểm tra lại");
                }

                string msg = Repository.UpdateRap(rap, 0);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentRap);

                return BadRequest(msg);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut("CapNhatThongTinRapChieu")]
        public IActionResult UpdateRapChieu(RapVM rap)
        {
            string msg = string.Empty;
            try
            {
                var currentRap = Repository.GetRapById(rap.maRap);

                if (currentRap == null)
                {
                    return BadRequest("Thông tin rạp này chưa được tạo trong hệ thống");
                }

                msg = Repository.UpdateRap(rap, 1);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentRap);

                return BadRequest(msg);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }

        [HttpDelete("XoaRapChieu")]
        public IActionResult DeleteRapChieu(int maRap)
        {
            string msg = string.Empty;

            if (maRap==0)
            {
                return BadRequest("Mã rạp không được bỏ trống");
            }

            RapVM rap = new RapVM
            {
                maRap = maRap
            };

            msg = Repository.UpdateRap(rap, 2);

            if (msg == Helpers.Type.SUCCESS) return Ok(rap);

            return BadRequest(msg);
        }

        [HttpGet("LayDanhSachTrangThaiRap")]
        public IActionResult GetListTypePhim()
        {
            var listTypeRap = Repository.GetFullTypeRap();

            if (listTypeRap == null) return BadRequest("Lấy danh sách trạng thái rạp thất bại! Xin vui lòng thử lại");

            return Ok(listTypeRap);
        }

        [HttpGet("LayThongTinCumRapChieu")]
        public IActionResult GetListCumRapChieu(string maCumRap)
        {
            if (!string.IsNullOrEmpty(maCumRap))
            {
                return Ok(Repository.GetCumRapByMCR(maCumRap));
            }

            return Ok(Repository.GetListCumRap());
        }

        [HttpPost("ThemCumRapChieu")]
        public IActionResult ThemCumRap(CumRapVM cumRAP)
        {
            try
            {
                var currentCumRap = Repository.GetCumRapByMCR(cumRAP.maCumRap);

                if (currentCumRap != null)
                {
                    return BadRequest("Thông tin cụm rạp này đã tồn tại trong hệ thống! Xin hãy kiểm tra lại");
                }

                string msg = Repository.UpdateCumRap(cumRAP, 0);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentCumRap);

                return BadRequest(msg);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut("CapNhatThongTinCumRapChieu")]
        public IActionResult UpdateCumRap(CumRapVM cumRAP)
        {
            string msg = string.Empty;
            try
            {
                var currentCumRap = Repository.GetCumRapByMCR(cumRAP.maCumRap);
                if (currentCumRap == null)
                {
                    return BadRequest("Thông tin cụm rạp này chưa được tạo trong hệ thống");
                }

                msg = Repository.UpdateCumRap(cumRAP, 1);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentCumRap);

                return BadRequest(msg);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }

        [HttpDelete("XoaCumRapChieu")]
        public IActionResult DeleteCumRap(string maCumRap)
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(maCumRap))
            {
                return BadRequest("Mã cụm rạp không được bỏ trống");
            }

            CumRapVM cumRap = new CumRapVM
            {
                maCumRap = maCumRap
            };

            msg = Repository.UpdateCumRap(cumRap, 2);

            if (msg == Helpers.Type.SUCCESS) return Ok(cumRap);

            return BadRequest(msg);
        }
    }
}
