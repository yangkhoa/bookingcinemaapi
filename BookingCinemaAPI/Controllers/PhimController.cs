﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingCinemaAPI.Models;
using BookingCinemaAPI.Services.Interface;
using BookingCinemaAPI.ViewModels;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;

namespace BookingCinemaAPI.Controllers
{
    [Route("api/QuanLyPhim")]
    [ApiController]
    public class PhimController : ControllerBase
    {
        private readonly IPhimService Repository;
        private IHostingEnvironment _hostingEnvironment;
        public PhimController(IPhimService repository, IHostingEnvironment hostingEnvironment)
        {
            this.Repository = repository;
            _hostingEnvironment = hostingEnvironment;
        }
        
        [HttpGet("LayDanhSachPhim")]
        public IActionResult GetListPhim(int maPhim)
        {
            if (maPhim>0)
            {
                var phim = Repository.GetPhimByCode(maPhim);
                return Ok(phim);
            }

            var listPhim = Repository.GetListPhim();
    
            if (listPhim==null) return BadRequest("Đã có lỗi xảy ra xin vui lòng thử lại sau!!!");

            return Ok(listPhim);
        }

        [HttpGet("LayDanhSachPhimDayDu")]
        public IActionResult GetFullListPhim()
        {
            var listPhim = Repository.GetFullListPhim();

            if (listPhim == null) return BadRequest("Đã có lỗi xảy ra xin vui lòng thử lại sau!!!");

            return Ok(listPhim);
        }

        [HttpGet("LayDanhSachPhimSapChieu")]
        public IActionResult GetListPhimComing(int maPhim)
        {
            var listPhimComming = Repository.GetListPhimComming();

            if (listPhimComming == null) return BadRequest("Đã có lỗi xảy ra xin vui lòng thử lại sau!!!");

            return Ok(listPhimComming);
        }

        [HttpGet("LayDanhSachHinhBanner")]
        public IActionResult GetBanner()
        {
            List<object> lisHinh = new List<object> {
                new { img = "https://s3img.vcdn.vn/123phim/2019/10/khung-hinh-15699048110729.jpg", trailer = "https://youtu.be/c24T0xJz4iU" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/10/ca-map-15701562560682.jpg", trailer = "https://youtu.be/gchh_Y8dwlg" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/10/troi-sang-15701562872972.jpg", trailer = "https://youtu.be/ikz2SFJyU1Q" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/10/joker-15701565726725.png", trailer = "https://youtu.be/K1-11dWJocM" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/08/angel-15671377090495.jpg", trailer = "https://www.youtube.com/watch?v=Mh2NcjEXHOg" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/08/anh-thay-15671389225425.jpg", trailer = "https://www.youtube.com/watch?v=_H9iM_JQAVU" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/09/buommmm-15677399454407.jpg", trailer = "https://www.youtube.com/watch?v=xhJ5P7Up3jA" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/10/ngot-ngao-15707600969282.jpg", trailer = "https://youtu.be/KUCVyo9IT3c" },
                new { img = "https://s3img.vcdn.vn/123phim/2019/10/eve-15707599495299.jpg", trailer = "https://youtu.be/YUTgFBJeYx0" },
            };
            Random rnd = new Random();

            return Ok(lisHinh.OrderBy(x=> rnd.Next()).Take(3));
        }

        [HttpPost("ThemPhim")]
        public IActionResult ThemPhim(PhimVM phim)
        {
            try
            {
                var currentPhim = Repository.CheckPhim(phim.maPhim);

                if (currentPhim != null)
                {
                    return BadRequest("Phim này đã tồn tại trong hệ thống! Xin hãy kiểm tra lại");
                }

                string msg = Repository.UpdatePhim(phim, 0);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentPhim);

                return BadRequest(msg);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPut("CapNhatPhim")]
        public IActionResult UpdatePhim (PhimVM phim)
        {
            try
            {
                var currentPhim = Repository.CheckPhim(phim.maPhim);

                if (currentPhim == null)
                {
                    return BadRequest("Phim này chưa có trong hệ thống!");
                }

                string msg = Repository.UpdatePhim(phim, 1);

                if (msg == Helpers.Type.SUCCESS) return Ok(currentPhim);

                return BadRequest(msg);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }

        [HttpDelete("XoaPhim")]
        public IActionResult DeletePhim(int maPhim)
        {
            string msg = string.Empty;

            if (maPhim == 0)
            {
                msg = "Mã phim không được bỏ trống";
                return BadRequest(msg);
            }

            PhimVM phim = new PhimVM
            {
                maPhim = maPhim
            };

            msg = Repository.UpdatePhim(phim, 2);

            if (msg == Helpers.Type.SUCCESS) return Ok(phim);

            return BadRequest(msg);
        }

        [HttpGet("LayDanhSachLoaiPhim")]
        public IActionResult GetListTypePhim()
        {
            var listTypePhim = Repository.GetFullTypePhim();

            if (listTypePhim == null) return BadRequest("Lấy danh sách loại phim thất bại! Xin vui lòng thử lại");

            return Ok(listTypePhim);
        }

        [Produces("application/json")]
        [HttpPost("UploadAnhPhim"), DisableRequestSizeLimit]
        public IActionResult UploadFile()
        {
            try
            {
                string fullPath = string.Empty;
                string fileName = string.Empty;
                var file = Request.Form.Files[0];
                string folderName = "upload";
                string rootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(rootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                string domainName = Request.Host.Value.ToString();

                string linkImg = "http://" + domainName + "/upload/" + fileName;

                return Ok(linkImg);
            }
            catch (System.Exception ex)
            {
                return BadRequest("Upload Failed: " + ex.Message);
            }
        }
    }
}
